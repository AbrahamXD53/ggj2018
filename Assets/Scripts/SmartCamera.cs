﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartCamera : MonoBehaviour
{

    // Use this for initialization
    private new Camera camera;

    public LayerMask[] mask;

    public List<int> foundPlayers;
    public List<int> trackedPlayers;


    void Start()
    {
        camera = GetComponent<Camera>();
    }

    public void SetPlayer(int player)
    {
        foundPlayers = new List<int>();
        trackedPlayers = new List<int>();
        trackedPlayers.Add(player);
        foundPlayers.Add(player);
        CalcMask();
    }

    public bool IsTracked(int player)
    {
        return trackedPlayers.Contains(player);
    }
    public bool IsWatchinOther()
    {
        return foundPlayers.Count > 1;
    }

    public void AddPlayer(int player)
    {
        if (foundPlayers.Contains(player))
        {
            foundPlayers.Add(player);
            CalcMask();
        }

    }
    public void AddTreckedPlayer(int player)
    {
        foundPlayers.Add(player);
        trackedPlayers.Add(player);
        CalcMask();

    }
    public void RemovePlayer(int player)
    {
        if (foundPlayers.Contains(player))
        {
            foundPlayers.Remove(player);
            trackedPlayers.Remove(player);
            CalcMask();
        }
    }

    void CalcMask()
    {
        if (foundPlayers.Contains(1))
        {
            if (foundPlayers.Contains(2))
            {
                if (foundPlayers.Contains(3))
                {
                    if (foundPlayers.Contains(4))
                    {
                        //everything
                        camera.cullingMask = mask[0];
                    }
                    else
                    {
                        //123
                        camera.cullingMask = mask[15];
                    }
                }
                else
                {
                    if (foundPlayers.Contains(4))
                    {
                        //124
                        camera.cullingMask = mask[14];
                    }
                    else
                    {
                        //12
                        camera.cullingMask = mask[13];
                    }
                }
            }
            else
            {
                if (foundPlayers.Contains(3))
                {
                    if (foundPlayers.Contains(4))
                    {
                        //134
                        camera.cullingMask = mask[12];

                    }
                    else
                    {
                        //13
                        camera.cullingMask = mask[11];
                    }
                }
                else
                {
                    if (foundPlayers.Contains(4))
                    {
                        //14
                        camera.cullingMask = mask[10];

                    }
                    else
                    {
                        //1
                        camera.cullingMask = mask[9];

                    }
                }
            }
        }
        else
        {
            if (foundPlayers.Contains(2))
            {
                if (foundPlayers.Contains(3))
                {
                    if (foundPlayers.Contains(4))
                    {
                        //234
                        camera.cullingMask = mask[8];
                    }
                    else
                    {
                        //23
                        camera.cullingMask = mask[7];
                    }
                }
                else
                {
                    if (foundPlayers.Contains(4))
                    {
                        //24
                        camera.cullingMask = mask[6];

                    }
                    else
                    {
                        //2
                        camera.cullingMask = mask[5];
                    }
                }
            }
            else
            {
                if (foundPlayers.Contains(3))
                {
                    if (foundPlayers.Contains(4))
                    {
                        //34
                        camera.cullingMask = mask[4];

                    }
                    else
                    {
                        //3
                        camera.cullingMask = mask[3];
                    }
                }
                else
                {
                    if (foundPlayers.Contains(4))
                    {
                        //4
                        camera.cullingMask = mask[2];

                    }
                    else
                    {
                        //nothing
                        camera.cullingMask = mask[1];
                    }
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {

    }


}
