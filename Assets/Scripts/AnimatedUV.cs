﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedUV : MonoBehaviour
{

    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";

    public int cols = 7;
    public int rows = 0;

    private int currentX = 0;
    private int currentY = 0;

    private Vector2 size;

    Vector2 uvOffset = Vector2.zero;

    public float speed = 1;

    private float ctime = 0;

    public new Renderer renderer;

    void Start()
    {
        size = new Vector2(1.0f / cols, 1.0f / rows);
        renderer.materials[materialIndex].SetTextureScale(textureName, size);
    }

    void LateUpdate()
    {
        if ((ctime += Time.deltaTime) >= speed)
        {
            currentX++;
            if (currentX > cols)
            {
                currentX = 0;
                currentY++;
                if (currentY > rows)
                {
                    currentY = 0;
                }
            }
			uvOffset=new Vector3(size.x*currentX,size.y*currentY);
            ctime = 0;
        }
        
        renderer.materials[materialIndex].SetTextureOffset(textureName, uvOffset);

    }
}
