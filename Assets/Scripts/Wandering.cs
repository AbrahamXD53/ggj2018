﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wandering : MonoBehaviour {

    private NavMeshAgent agent;
    public float updateRate=2;
    public Vector2 range;

	void Start ()
    {
        //agent.updateRotation = true;
        agent=GetComponent<NavMeshAgent>();
        StartCoroutine(SetPosition());
    }

    IEnumerator SetPosition()
    {
        yield return new WaitForSeconds(updateRate);
        var radius = Random.insideUnitCircle * Random.Range(range.x, range.y);
        
        agent.SetDestination(transform.position+new Vector3(radius.x, 0, radius.y));
        StartCoroutine(SetPosition());
        agent.updateRotation = true;
        agent.updatePosition = true;
    }
    private void OnDrawGizmosSelected()
    {
        if (agent)
        {
            if (agent.destination!=null)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(agent.destination, .5f);
            }
        }
    }
}
