﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{

    // Use this for initialization
    public List<Color> playerColors;

    public Color defaultColor;
    public GameObject prefab;
    public Transform parent;

    private Dictionary<int, GameObject> players;

    public List<int> freespaces = new List<int>() { 0, 1, 2, 3 };


    public int sceneToloadWhenReady = 0;

    public Sprite[] controllerSprites;

    public Sprite[] characters;

    public string[] names;

    public float[] live;
    public float[] speed;

    void Start()
    {
        players = new Dictionary<int, GameObject>();

        PartyController.PlayerAdded += PlayerAdded;
        PartyController.PlayerRemoved += PlayerRemoved;
        PartyController.PlayerReady += PlayerReady;
        PartyController.PlayerNotReady += PlayerNotReady;
        PartyController.AllPlayersReady += AllPlayersReady;
        PartyController.PlayerCharacterChanged+=PlayerCharacterChanged;
    }
    void PlayerReady(int playerIndex)
    {
        if (players.ContainsKey(playerIndex))
        {
            players[playerIndex].GetComponent<Image>().color = playerColors[playerIndex];
            
        }
    }
    void PlayerCharacterChanged(int playerIndex){
        if(players.ContainsKey(playerIndex)){
            players[playerIndex].transform.GetChild(0).GetComponent<Image>().sprite= 
                characters[PartyController.Instance.players[playerIndex].selectedCharacter];
            players[playerIndex].transform.GetChild(1).GetComponent<Text>().text=names[PartyController.Instance.players[playerIndex].selectedCharacter];
            players[playerIndex].transform.GetChild(3).GetComponent<Slider>().value=live[PartyController.Instance.players[playerIndex].selectedCharacter];
            players[playerIndex].transform.GetChild(5).GetComponent<Slider>().value=speed[PartyController.Instance.players[playerIndex].selectedCharacter];
            players[playerIndex].transform.GetChild(7).GetComponent<Slider>().value=0.5f;
        }
    }
    void PlayerNotReady(int playerIndex)
    {
        if (players.ContainsKey(playerIndex))
        {
            var tempColor = playerColors[playerIndex];
            tempColor.a = 0.4f;
            players[playerIndex].GetComponent<Image>().color = tempColor;
        }
    }
    void PlayerAdded(int playerIndex)
    {
        var go = Instantiate(prefab, parent);
        go.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = PartyController.Instance.players[playerIndex].useKeyboard ? controllerSprites[controllerSprites.Length - 1] : controllerSprites[(int)PartyController.Instance.players[playerIndex].controlIndex - 1];
        players.Add(playerIndex, go);
    }
    void PlayerRemoved(int playerIndex)
    {
        RefreshUI();
    }

    void RefreshUI()
    {
        for (int i = 0; i < players.Count; i++)
        {
            var go = players[i];
            players.Remove(i);
            Destroy(go);
            if (i < PartyController.Instance.players.Count)
            {
                PlayerAdded(PartyController.Instance.players[i].realIndex);
                if (PartyController.Instance.players[i].ready)
                    PlayerReady(PartyController.Instance.players[i].realIndex);
                else
                    PlayerNotReady(PartyController.Instance.players[i].realIndex);
            }
        }
    }
    void AllPlayersReady()
    {
        PartyController.PlayerAdded -= PlayerAdded;
        PartyController.PlayerRemoved -= PlayerRemoved;
        PartyController.AllPlayersReady -= AllPlayersReady;
        PartyController.PlayerReady -= PlayerReady;
        PartyController.PlayerNotReady -= PlayerNotReady;
        PartyController.PlayerCharacterChanged-=PlayerCharacterChanged;
        SceneManager.LoadScene(sceneToloadWhenReady);
    }

}
