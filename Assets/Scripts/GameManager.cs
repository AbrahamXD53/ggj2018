﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public List<Transform> _spawnPoints;
    public List<PlayerInput> _playerInputList;
    public List<PlayerController> _playerControllerList;

    public List<Transform> _submarinesPrefabs;

    public List<Camera> _cameraSettingsList;

    // Use this for initialization
    void Awake()
    {

    }

    // Use this for initialization
    void Start() {
        AssignPlayers();
    }

    // Update is called once per frame
    void Update() {
        //if only one player is alive
        /*if()
        GameOver();*/
    }

    /// <summary>
    /// Active players
    /// </summary>
    public void AssignPlayers()
    {
        if (PartyController.Instance != null)
        {
            _playerInputList = PartyController.Instance.players;

            //Instance players to spawn points
            for (int i = 0; i < _playerInputList.Count; i++)
            {
                //Instantiate(_playerInputList[i].Selectedcharacter, _spawnPoints[i]);
            }
        }
    }

    /// <summary>
    /// The game ends
    /// </summary>
    public void GameSet(){
        //Pause the game
        Time.timeScale = 0;

        //Declare a winner
        ShowWinner();

        //Show Game Set Window
        //Show retry button
        //Show quit button
    }

    /// <summary>
    /// Show the player winner
    /// </summary>
    public void ShowWinner()
    {
        //Show winner in UI
    }

}
