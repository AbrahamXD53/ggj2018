﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToHngar : MonoBehaviour {

	public GameObject cameraPrefab;
	/*public Rect[][] camRecs={
		new Rect[]{ new Rect(0,0,1,1), },
		new Rect[]{ new Rect(0,0.5f,1,0.5f),new Rect(0,0,1,0.5f) },
		new Rect[]{ new Rect(0,0.5f,1,0.5f),new Rect(0,0,0.5f,0.5f),new Rect(0.5f,0,0.5f,0.5f) },
		new Rect[]{ new Rect(0,0.5f,1,0.5f),new Rect(0.5f,0.5f,0.5f,0.5f),new Rect(0,0,0.5f,0.5f),new Rect(0.5f,0,0.5f,0.5f) },
	};*/
	public Rect[][] camRecs={
		new Rect[]{ new Rect(0,0,1,1), },
		new Rect[]{ new Rect(0,0,0.5f,1f),new Rect(0.5f,0,0.5f,1f) },
		new Rect[]{ new Rect(0,0,0.5f,1f),new Rect(0.5f,0.5f,0.5f,0.5f),new Rect(0.5f,0,0.5f,0.5f) },
		new Rect[]{ new Rect(0,0.5f,1,0.5f),new Rect(0.5f,0.5f,0.5f,0.5f),new Rect(0,0,0.5f,0.5f),new Rect(0.5f,0,0.5f,0.5f) },
	};
	public LayerMask[] masks={

	};

	public int[] palyerMasks;

	public Transform[] spawnPoints;

	public static void SetLayerRecursively(GameObject go, int layerNumber) {
        if (go == null) return;
			foreach (Transform trans in go.GetComponentsInChildren<Transform>(true)) {
				trans.gameObject.layer = layerNumber;
			}
    }
	void Start () {
		PlayerController.players=new List<PlayerController>();
		for (int i = 0; i < PartyController.Instance.players.Count; i++)
		{
			var go= Instantiate(cameraPrefab,spawnPoints[Random.Range(0,spawnPoints.Length)]);
			var camera=go.GetComponentInChildren<Camera>();
			camera.rect=camRecs[PartyController.Instance.players.Count-1][i];
			camera.cullingMask=masks[i];
			SetLayerRecursively(go.transform.GetChild(1).gameObject,palyerMasks[i]);
			var pow=go.GetComponentInChildren<PlayerController>();
			pow.playerInput=PartyController.Instance.players[i];
		}
		
	}
	
}
