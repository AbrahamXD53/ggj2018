﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animatron300 : MonoBehaviour
{

    [Serializable]
    public class Motion
    {
        public bool Eneable = false;
        public Vector3 vector;
        public float speed;
        public bool useCos;
        public float amplitude, frecuency;

        public bool captureInitial = true;

        public Vector3 offset;

        public Vector3 temp;

        public Vector3 Update()
        {
            if (useCos)
                return temp + offset + vector * (amplitude * Mathf.Cos(frecuency * Time.realtimeSinceStartup) * speed);
            return temp + offset + vector * (speed * Time.realtimeSinceStartup);
        }
    }

    public Motion rotation;
    public Motion movement;
    public Motion scale;

    private void Start()
    {
        if (rotation.captureInitial)
            rotation.temp = transform.rotation.eulerAngles;
        if (movement.captureInitial)
            movement.temp = transform.position;
        if (scale.captureInitial)
            scale.temp = transform.localScale;
    }

    void Update()
    {
        if (movement.Eneable)
            transform.position = movement.Update();
        if (rotation.Eneable)
            transform.rotation = Quaternion.Euler(rotation.Update() * Mathf.Rad2Deg);
        if (scale.Eneable)
            transform.localScale = scale.Update();
    }
}
