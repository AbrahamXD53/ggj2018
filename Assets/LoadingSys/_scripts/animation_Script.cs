﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class animation_Script : MonoBehaviour
{
	Animator anim;
	public int n_scena;
	private int progresN=0;
	public float waitTime;
    public int a_init;
    public int a_out;

    void Start()
	{
		anim = GetComponent<Animator>();
        a_init = UnityEngine.Random.RandomRange(1,5);
        a_out = UnityEngine.Random.RandomRange(1, 5);
        anim.SetInteger("anim_out",a_init);
    }

	public void animacion()
	{
        anim.SetInteger("anim_in", a_out);
        Invoke("cargar",waitTime);
	}

	public void cargar()
	{
		StartCoroutine(DisplayLoadingScreen(n_scena));
	}

	IEnumerator DisplayLoadingScreen(int level)
	{
		AsyncOperation async = SceneManager.LoadSceneAsync(level);
		//progreso.text="Cargando "+progresN+"%";
		while(!async.isDone)
		{
			progresN=(int)(async.progress*100);
			//progreso.text="Cargando "+progresN+"%";
			yield return null;
		}
	}
}